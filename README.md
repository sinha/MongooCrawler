### 联系方式
blog：http://blog.csdn.net/flashflight
<br>
email:jeff_dev@sina.com

### MongooCrawler 

一款开源分布式爬虫软件，如果喜欢这个项目记得加star哈。
<br>
MongooCrawler为了解决抓取速度和持久化速度不一致的问题引入了disruptor并发框架
<br>
抓取的数据分为两类，一类是文本，一类是url，url暂时使用MD5进行去重，再放入队列文本则会直接进入数据库。
<br>
如果想私下交流可以在我blog留言
<br>
### MongooCrawler的优势
<br>
1、分布式
<br>
参考[Spiderman](http://git.oschina.net/l-weiwei/Spiderman2)的实现正是基于redis的，但是这样redis就成为一个单点问题，那么如果网络出问题，或者redis服务器宕机会造成部分数据无法爬取这样降低用户体验，mongoocrawler参照redis cluster实现真集群部署，节点之间可以相互通讯，当部分节点无法访问时也不影响爬虫的可用性。
<br>
2、验证码
<br>
这些框架并没有提供验证码的解决方案，MongooCrawler会根据作者的亲身经历提供一些解决方案
<br>
3、cookie
<br>
cookie提供自动跟踪的开关，流行的部分开源爬虫框架并不能有效解决sso单点登录中跨域携带cookie的问题，MongooCrawler会提供解决方案。
<br>
4、反爬策略
<br>
流行的部分爬虫框架没有提供简单易用的访问频次的控制，必须依赖用户手动硬编码调用函数控制。user-agent等头信息也没提供简单易用的管理接口，完全依赖用户硬编码。
<br>
5、高性能
<br>
流行的部分爬虫框架并没有实现弹性控制，没有根据不同节点的性能差异有效利用不同节点的硬件资源，没有做熔断机制和监控，这些都是MongooCrawler会做的
<br>

### 更新说明


2017-07-02 增加对数据库连接池druid的支持，提高mysql数据库存储效率
<br>
2017-05-03 完成图书爬虫逻辑
<br>
2017-04-16 添加了电商图书类商品爬取逻辑
<br>
2017-01-12 添加了解析html的逻辑，依赖jsoup包
<br>
2017-04-12 添加了一个事例
<br>
2017-05-24 开发监控端
<br>
2018-09-17 已开发完第一版监控端
<br>
2018-12-12 提供部署文档
<br>
2020-04-05 开始集群支持-心跳检测功能开发

### 下一步计划
<br>1、集群监控

### 怎么部署项目？
MongooCrawler的部署文件在/docs/MongooCrawler部署文件.docx中
<br>
MongooCrawler-Monitor（python监控端）已写