package com.trytech.mongoocrawler.client;

import com.trytech.mongoocrawler.client.transport.tcp.NettyTcpClient;

/**
 * Created by coliza on 2017/10/7.
 */
public class CrawlerContext {

    private CrawlerContext(){}

    private NettyTcpClient nettyTcpClient;

    public static CrawlerContext newInstance(){
        CrawlerContext crawlerContext = new CrawlerContext();
        return crawlerContext;
    }

    public void start(){
        //启动客户端
        nettyTcpClient = NettyTcpClient.newInstance("127.0.0.1", 8889);
        //初始化ProtocolHandler链
        nettyTcpClient.addHandler(new UrlProtocolHandler());
        nettyTcpClient.addHandler(new CommandProtocolHandler());
        //启动客户端
        nettyTcpClient.start();
    }

    public void close() {
        if (nettyTcpClient != null) {
            nettyTcpClient.close();
        }
    }
}
