package com.trytech.mongoocrawler.server;

import org.junit.Test;

/**
 * Created by hp on 2017-2-17.
 */
public class CrawlerContextTest {
    @Test
    public void testContextStart() throws Exception {
        CrawlerContext context = CrawlerContext.getCrawlerContext();
        context.start();
    }
}
