package com.trytech.mongoocrawler.server.transport.handler;

import com.trytech.mongoocrawler.common.transport.protocol.AbstractProtocol;
import com.trytech.mongoocrawler.common.transport.protocol.CommandProtocol;
import com.trytech.mongoocrawler.common.transport.protocol.ProtocolFilterChain;
import com.trytech.mongoocrawler.server.CrawlerContext;
import com.trytech.mongoocrawler.server.transport.handler.executor.AbstractCommandExecutor;
import com.trytech.mongoocrawler.server.transport.handler.executor.CommandExecutorFactory;

/**
 * @author jiangtao@meituan.com
 * @since: Created on 2018年07月01日
 */
public class CommandProtocolHandler extends ServerProtocolHandler {

    public CommandProtocolHandler(CrawlerContext crawlerContext) {
        super(crawlerContext);
    }

    @Override
    public AbstractProtocol doHandle(ProtocolFilterChain filterChain, AbstractProtocol protocol) throws Exception {
        CommandProtocol.Command command = ((CommandProtocol) protocol).getCommand();
        AbstractCommandExecutor executor = CommandExecutorFactory.create(command);
        if (executor != null) {
            return executor.execute(command, crawlerContext, protocol);
        }
        return filterChain.doFilter(protocol);
    }

    @Override
    protected boolean checkProtocol(AbstractProtocol abstractProtocol) {
        return abstractProtocol instanceof CommandProtocol;
    }
}