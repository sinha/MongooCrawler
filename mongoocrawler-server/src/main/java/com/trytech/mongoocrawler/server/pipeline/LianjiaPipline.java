package com.trytech.mongoocrawler.server.pipeline;

import com.trytech.mongoocrawler.server.common.db.MySqlDruidDataSource;
import com.trytech.mongoocrawler.server.entity.LianjiaItem;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by coliza on 2017/6/15.
 */
public class LianjiaPipline extends MysqlPipeline<List<LianjiaItem>> {
    @Override
    public void store(List<LianjiaItem> lianjiaItemList) throws SQLException, ClassNotFoundException {
        MySqlDruidDataSource dataSource = (MySqlDruidDataSource) getDataSource();
        for (LianjiaItem lianjiaItem : lianjiaItemList) {
            String sql = "insert into lj_house(`C_TITLE`,`C_LOCATION`,`C_TYPE`,`C_FLOORSPACE`,`C_PRICE`,`C_UNIT_PRICE`) values('" + lianjiaItem.getTitle() + "','" + lianjiaItem.getLocation() + "','" + lianjiaItem.getType() +
                    "','" + lianjiaItem.getFloorSpace() + "'," + lianjiaItem.getPrice().toString() + "," + lianjiaItem.getUnitPrice().toString() + ")";
            dataSource.insert(sql);
        }
    }
}