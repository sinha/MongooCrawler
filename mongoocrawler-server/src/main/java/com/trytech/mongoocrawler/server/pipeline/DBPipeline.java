package com.trytech.mongoocrawler.server.pipeline;

import com.trytech.mongoocrawler.server.common.db.CrawlerDataSource;

/**
 * Created by coliza on 2018/9/18.
 */
public abstract class DBPipeline<T> extends AbstractPipeline<T> {
    public abstract CrawlerDataSource getDataSource();
    protected abstract String getDatasourceName();
}