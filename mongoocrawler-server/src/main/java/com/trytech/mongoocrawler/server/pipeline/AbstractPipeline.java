package com.trytech.mongoocrawler.server.pipeline;

import java.sql.SQLException;

/**
 * Created by coliza on 2017/6/15.
 */
public abstract class AbstractPipeline<T> {
    public abstract void store(T t) throws SQLException, ClassNotFoundException;
}
