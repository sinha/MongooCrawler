package com.trytech.mongoocrawler.server.transport.handler.executor;

import com.trytech.mongoocrawler.common.bean.MonitorData;
import com.trytech.mongoocrawler.common.enums.CrawlerStatus;
import com.trytech.mongoocrawler.common.transport.protocol.AbstractProtocol;
import com.trytech.mongoocrawler.common.transport.protocol.CommandProtocol;
import com.trytech.mongoocrawler.common.transport.protocol.MonitorProtocol;
import com.trytech.mongoocrawler.server.CrawlerContext;
import com.trytech.mongoocrawler.server.CrawlerSession;
import com.trytech.mongoocrawler.server.common.enums.LifeCycle;
import com.trytech.mongoocrawler.server.parser.lianjia.LianjiaHtmlParser;
import com.trytech.mongoocrawler.server.transport.UrlParserPair;

/**
 * Created by coliza on 2020/2/11.
 */
public class MonitorExecutor implements AbstractCommandExecutor {

    @Override
    public AbstractProtocol execute(CommandProtocol.Command command, CrawlerContext crawlerContext, AbstractProtocol protocol) {
        //从url池中获取url
        MonitorProtocol monitorProtocol = new MonitorProtocol();
        monitorProtocol.setTraceId(protocol.getTraceId());
        MonitorData monitorData = MonitorData.getInstance();
        monitorProtocol.setMonitorData(monitorData);
        //如果是爬取任务
        CrawlerSession crawlerSession = crawlerContext.getSession(protocol
                .getSessionId());
        monitorProtocol.setSessionId(crawlerSession.getSessionId());
        LifeCycle lifeCycle = crawlerSession.getLifeCycle();
        monitorData.getServerConfig().setRunStatus(CrawlerStatus.RUNNING.getCode());
        monitorData.getServerConfig().setRunStatusLabel(CrawlerStatus.RUNNING.getValue());
        if (!lifeCycle.isFree()) {
            return monitorProtocol;
        }
        monitorData.getServerConfig().setRunStatus(CrawlerStatus.STOPPED.getCode());
        monitorData.getServerConfig().setRunStatusLabel(CrawlerStatus.STOPPED.getValue());
        String url = ((CommandProtocol) protocol).getData().toString();
        UrlParserPair urlParserPair = new UrlParserPair(url, new LianjiaHtmlParser());
        crawlerSession.pushUrl(urlParserPair);

        return monitorProtocol;
    }
}
