package com.trytech.mongoocrawler.server.common.db;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.pool.DruidDataSourceFactory;
import com.trytech.mongoocrawler.server.common.exception.DataSourceInitException;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Druid连接Mysql的连接池
 */
public class MySqlDruidDataSource extends CrawlerDataSource {
    private DruidDataSource dataSource;
    private Connection conn;
    @Override
    public void init() throws DataSourceInitException {
        try {
            dataSource = (DruidDataSource)DruidDataSourceFactory.createDataSource(propertiesMap);
        } catch (Exception e) {
            throw new DataSourceInitException("Mysql DataSource initialization get failed!");
        }
    }

    private Connection getConnection() throws DataSourceInitException {
            try {
                conn = dataSource.getConnection();
            } catch (SQLException e) {
                throw new DataSourceInitException("Mysql DataSource initialization get failed!");
            }
        return conn;
    }
    @Override
    public void destory() {
        if(dataSource != null){
            dataSource.close();
        }
    }

    @Override
    public void closeConnection() {
        try {
            Connection connection = getConnection();
            connection.close();
        } catch (DataSourceInitException | SQLException e) {
            e.printStackTrace();
        }
    }

    public void insert(String sql){
        Connection connection = null;
        Statement statement = null;
        try {
            connection = getConnection();
            statement = connection.createStatement();
            statement.execute(sql);
        } catch (DataSourceInitException | SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                statement.close();
            }catch (SQLException e){
                e.printStackTrace();
            }
        }
    }
}
