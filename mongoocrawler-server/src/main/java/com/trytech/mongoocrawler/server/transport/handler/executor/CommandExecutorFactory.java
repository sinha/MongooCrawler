package com.trytech.mongoocrawler.server.transport.handler.executor;

import com.trytech.mongoocrawler.common.transport.protocol.CommandProtocol;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by coliza on 2020/2/11.
 */
public class CommandExecutorFactory {
    private static Map<CommandProtocol.Command, AbstractCommandExecutor> executorMap = new HashMap<CommandProtocol.Command, AbstractCommandExecutor>();

    static {
        executorMap.put(CommandProtocol.Command.GET_URL, new GetUrlExecutor());
        executorMap.put(CommandProtocol.Command.HEARTBEAT, new HeartBeatExecutor());
        executorMap.put(CommandProtocol.Command.MONITOR, new MonitorExecutor());
    }

    public static AbstractCommandExecutor create(CommandProtocol.Command command) {
        return executorMap.get(command);
    }
}
