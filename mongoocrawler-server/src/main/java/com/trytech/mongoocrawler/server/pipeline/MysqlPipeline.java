package com.trytech.mongoocrawler.server.pipeline;

import com.trytech.mongoocrawler.server.CrawlerContext;
import com.trytech.mongoocrawler.server.common.db.CrawlerDataSource;

/**
 * Created by coliza on 2018/9/18.
 */
public abstract class MysqlPipeline<T> extends DBPipeline<T> {
    @Override
    protected String getDatasourceName() {
        return "mysql";
    }

    @Override
    public CrawlerDataSource getDataSource() {
        return CrawlerContext.getConfig().getConfigBean().getDataSource(getDatasourceName());
    }
}
