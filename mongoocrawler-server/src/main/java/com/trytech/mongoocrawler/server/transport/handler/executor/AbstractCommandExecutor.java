package com.trytech.mongoocrawler.server.transport.handler.executor;

import com.trytech.mongoocrawler.common.transport.protocol.AbstractProtocol;
import com.trytech.mongoocrawler.common.transport.protocol.CommandProtocol;
import com.trytech.mongoocrawler.server.CrawlerContext;

/**
 * Created by coliza on 2020/2/11.
 */
public interface AbstractCommandExecutor {
    AbstractProtocol execute(CommandProtocol.Command command, CrawlerContext crawlerContext, AbstractProtocol protocol);
}