package com.trytech.mongoocrawler.server.transport.handler.executor;

import com.trytech.mongoocrawler.common.transport.protocol.AbstractProtocol;
import com.trytech.mongoocrawler.common.transport.protocol.CommandProtocol;
import com.trytech.mongoocrawler.common.transport.protocol.UrlProtocol;
import com.trytech.mongoocrawler.server.CrawlerContext;
import com.trytech.mongoocrawler.server.DistributedCrawlerSession;
import com.trytech.mongoocrawler.server.transport.UrlParserPair;

/**
 * Created by coliza on 2020/2/11.
 */
public class GetUrlExecutor implements AbstractCommandExecutor {

    @Override
    public AbstractProtocol execute(CommandProtocol.Command command, CrawlerContext crawlerContext, AbstractProtocol protocol) {
        //获取session
        DistributedCrawlerSession crawlerSession = (DistributedCrawlerSession) crawlerContext.getSession(protocol.getSessionId());

        UrlParserPair urlParserPair = crawlerSession.fetchUrl();
        //从url池中获取url
        UrlProtocol urlProtocol = new UrlProtocol();
        urlProtocol.setTraceId("001");
        urlProtocol.setUrl(urlParserPair.getUrl());
        return urlProtocol;
    }
}
